/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs').promises
const path = require('path')
const randomFiles = path.join(__dirname, './-randomFiles.js')

function createFiles() {
    let promises = []
    try {
        for (let index = 0; index < 5; index++) {

            let promise = fs.writeFile(`${index}randomFiles`, 'w', "utf8")
                .then(() => console.log("file created"))
                .catch((err) => console.log(err))
                promises.push(promise)

        }
        return Promise.all(promises)


    } catch (err) {
        console.log(err)
    }
}


function deleteFiles() {
    let promises =[]
    try {
        for (let index = 0; index < 5; index++) {

          let promise=  fs.unlink(`${index}randomFiles`)
            .then(()=>console.log('file removed'))
            .catch((err)=>console.log(err))
            promises.push(promise)
        }
        return Promise.all(promises)

    } catch (err) {
        console.log(err)
    }
}




module.exports = { createFiles, deleteFiles };