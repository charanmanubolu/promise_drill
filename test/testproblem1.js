const  { createFiles, deleteFiles }= require("../problem1.js")

createFiles()
    .then(() => deleteFiles())
    .catch((err)=>console.log(err))
