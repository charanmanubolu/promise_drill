const data= ('./lipsum_1.txt')
const files = ['./UpperCaseFile.txt', './lowerCaseFile.txt', './sortedData.txt'];
const {problem2,deleteFiles}=require('../problem2')

problem2(data)
    .then(()=>deleteFiles(files))
    .catch((err)=>console.log(err))
    