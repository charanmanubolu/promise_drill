/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs').promises


function problem2(data) {
  try {
    return fs.readFile(data, 'utf8',)// Reading the given file lipsum.txt

      //Converting the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
      .then(data => {
        let uppertext = data.toUpperCase()
        return fs.writeFile('./UpperCaseFile.txt', uppertext)
          .then(() => console.log('converted the text into upper case'))
          .then(() => uppertext)
          .catch((err) => console.log(err))

      })

      //3. Read the new file and convert it to lower case. Then split the contents into sentences. 
      //Then write it to a new file. Store the name of the new file in filenames.txt
      .then(uppertext => {
        let lowerCaseData = uppertext.toLocaleLowerCase()
        let sentences = lowerCaseData.split(/[.!?]/);
        let sentencesContent = sentences.join('\n\n');

        return fs.writeFile('./lowerCaseFile.txt', sentencesContent, 'utf8')
          .then(() => console.log("converted into upper case to lower case ans splited and filtered "))
          .then(() => sentencesContent)
          .catch((err) => console.log(err))
      })
      // 4. Read the new files, sort the content, write it out to a new file. 
      //Store the name of the new file in filenames.txt
      .then(sentencesContent => {
        let sortSentences = sentencesContent.split('/n/n').sort().join('/n/n')
        return fs.writeFile('./sortedData.txt', sortSentences, 'utf8')
          .then(() => console.log("data sorted"))
          .catch((err) => console.log(err))
      })
  } catch (err) {
    console.log(err)
  }

}


function deleteFiles(files) {
  let promises = []
  const deletePromises = files.map(file => {
    return fs.unlink(file, 'utf8')
      .then(() => console.log(`Deleted the file: ${file}`))
      .catch(err => console.error(err));
  });
  promises.push(deletePromises)
  return Promise.all(promises)
}


module.exports = { problem2, deleteFiles }